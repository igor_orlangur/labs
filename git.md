# git

# GIT is VCS

Version control is a system that records changes to a file or set of files over time so that you can recall specific versions later.

## Types of VCS

* Local Version Control Systems
* Centralized Version Control Systems
* Distributed Version Control Systems

![Distributed VCS] (https://git-scm.com/book/en/v2/images/distributed.png)

…created by Linus Torvalds in 2005 for development of the Linux kernel.

>GIT is a free and open source distributed version control system
>   designed to handle everything from small to very large projects
>      with lightning fast performance, speed and efficiency
>and it is easy to learn…
>
